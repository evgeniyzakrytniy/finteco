<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Ліцензія та сертифікати';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="entry-summary">
						<div class="sertificates">
<p><a href="http://finteco.com.ua/wp-content/uploads/2014/08/Finteko_license_22.pdf"><br />
<img src="http://finteco.com.ua/wp-content/uploads/2017/05/2017-05-03-15_22_30-Во-всю-страницу.png" alt="" /><br />
Ліцензія НБУ № 22 від 22.11.2014<br />
</a></p>
<p><a href="http://finteco.com.ua/wp-content/uploads/2018/03/Фінансова_звітність_-та_Аудиторський_звіт_за_2017_рік_ТОВ-«ФІНТЕКО».pdf"><br />
<img src="http://finteco.com.ua/wp-content/uploads/2017/05/2017-05-03-15_22_55-Фин.-отчетность-2016.pdf.png" alt="" /><br />
Фінансова звітність та Аудиторський звіт за 2017 рік<br />
</a></p>
<p><a href="http://finteco.com.ua/wp-content/uploads/2017/04/Фин.-отчетность-2016.pdf"><br />
<img src="http://finteco.com.ua/wp-content/uploads/2017/05/2017-05-03-15_22_55-Фин.-отчетность-2016.pdf.png" alt="" /><br />
Фінансова звітність та Аудиторський звіт за 2016 рік<br />
</a></p>
<p><a href="http://finteco.com.ua/wp-content/uploads/2014/08/Звіт_Фінтеко_2015.pdf"><br />
<img src="http://finteco.com.ua/wp-content/uploads/2017/05/2017-05-03-15_23_18-Звіт_Фінтеко_2015.pdf.png" alt="" /><br />
Фінансова звітність та Аудиторський звіт за 2015 рік<br />
</a></p>
<p><a href="http://finteco.com.ua/wp-content/uploads/2014/08/Finteko_report_2014.pdf"><br />
<img src="http://finteco.com.ua/wp-content/uploads/2017/05/2017-05-03-15_23_47-Finteko_report_2014.pdf.png" alt="" /><br />
Фінансова звітність та Аудиторський звіт за 2014 рік<br />
</a></p>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
		</div><!-- .entry-summary -->


</div>
