<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = $page_title;;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

        <div>
            <strong>Адреса:</strong> 03150, м.Київ, ВУЛИЦЯ ЧЕРВОНОАРМІЙСЬКА, будинок 55
        </div>

        <div>
            <strong>Поштова адреса:</strong> Україна, 01019, м.Київ, а/с 27
        </div>

        <div>
            <strong>Телефон:</strong> +38 044 591-19-74
        </div>

        <div>
            <strong>E-mail:<strong> fin@finteco.com.ua
        </div>

</div>
