<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Licence */

$this->title = 'Update Licence: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Licences', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="licence-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
