<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Licence */

$this->title = 'Create Licence';
$this->params['breadcrumbs'][] = ['label' => 'Licences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licence-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
