<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "licences".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $source
 */
class Licence extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $file;

    public static function tableName()
    {
        return 'licences';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
            [['title', 'alias', 'source'], 'required'],
            [['title', 'alias', 'source'], 'string', 'max' => 122],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'file' => 'File',
        ];
    }
}
